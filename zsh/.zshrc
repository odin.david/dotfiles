#!/bin/zsh

for file in $HOME/.zsh/rc/[0-9]*; do
  source "$file"
done
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
